const THOwnerProxy = artifacts.require("THOwnerProxy");
const { constants, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');
const {StepRecorder} = require("../util.js");

contract("OwnerProxy", (accounts) => {
  let naiveOwner;
  const hash1 = "0x68617368310000000000000000000000000000000000000000000000000000";
  const hash2 = "0x68617368320000000000000000000000000000000000000000000000000000";
  const hash3 = "0x68617368330000000000000000000000000000000000000000000000000000";

  let owneraddress;
  before(async () => {
    sr = StepRecorder('ganache', 'owner')
    naiveOwner = await THOwnerProxy.at(sr.read('owner-proxy'))
    await naiveOwner.grantRole(await naiveOwner.INITIALIZER_ROLE(), accounts[0])
  });

  describe("test init Ownership", async () => {
    it("check whether the map of hash to address is properly stored", async () => {
      await naiveOwner.initOwnerOf(hash1,accounts[1], { from: accounts[0] });
      owneraddress = await naiveOwner.ownerMap(hash1);
      assert.equal(owneraddress,accounts[1],"The init should be unsuccessful")
      });
    it("init the ownership with the same hash, should be reverted", async () => {
      await expectRevert(naiveOwner.initOwnerOf(hash1,accounts[1], { from: accounts[0] }),"Already initialized");
    });
    it("init the ownership by account[1], which is not the admin, should be reverted", async () => {
      await expectRevert(naiveOwner.initOwnerOf(hash3,accounts[3], { from: accounts[1] }),"AccessControl");
    });
  })
  describe("test ownerOf function", async () => {
    it("retrive the owner address with hash1, should be account[1]", async () => {
      owneraddress =await naiveOwner.ownerOf(hash1, { from: accounts[1] });
      assert.equal(owneraddress,accounts[1],"should show the address of account[1]")
      });
    it("retrive the owner address with hash2, should revert", async () => {
      await expectRevert(naiveOwner.ownerOf(hash2, { from: accounts[1] }),"This hash doesn't exist");
    });
  })

  describe("test transferOwnership function", async () => {
    before("add ownership with hash3 - account[3]", async () => {
      await naiveOwner.initOwnerOf(hash3,accounts[3], { from: accounts[0] });
    });
    it("transfer the ownership of hash3 to account[2]", async () => {
      await naiveOwner.transferOwnership(hash3,accounts[2], { from: accounts[3] });
      owneraddress = await naiveOwner.ownerMap(hash3);
      assert.equal(owneraddress,accounts[2],"The address of hash3 now should be account[2]")
      });
    it("account[3] want to transfer the ownership of hash3 to account[1],should revert, cause the owner of hash3 is now account[2]", async () => {
      await expectRevert(naiveOwner.transferOwnership(hash3,accounts[1], { from: accounts[3] })
                        , "The caller is not the owner");
    });
    it("transfer the ownership of hash2 to account[2], but the hash2 hasn't been init", async () => {
      await expectRevert(naiveOwner.transferOwnership(hash2,accounts[2], { from: accounts[3] })
                        , "This hash doesn't exist");
    });
  })
  describe("test change admin", async()=>{
    it("transfer admin", async()=>{
      await naiveOwner.transferAdministrator(accounts[1], {from:accounts[0]})
      await expectRevert(naiveOwner.transferAdministrator(accounts[1], {from:accounts[0]}), "AccessControl");
      await naiveOwner.grantRole(await naiveOwner.INITIALIZER_ROLE(), accounts[1], {from:accounts[1]});
      await naiveOwner.transferAdministrator(accounts[0], {from:accounts[1]})
    })
  })
});

