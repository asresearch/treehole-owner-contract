const THOwnerProxy = artifacts.require("THOwnerProxy");
const {DHelper, StepRecorder} = require("../util.js");
const { deployProxy } = require('@openzeppelin/truffle-upgrades');


async function performMigration(deployer, network, accounts, dhelper) {
  owner_proxy = await deployProxy(THOwnerProxy, [], {deployer});
  sr = StepRecorder(network, 'owner')
  sr.write('owner-proxy', owner_proxy.address)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
